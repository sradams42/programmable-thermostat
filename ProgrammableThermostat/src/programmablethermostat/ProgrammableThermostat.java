/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import java.util.Scanner;
import javafx.application.Application;
import static javafx.application.Application.launch;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author Samantha Adams'
 * 02-13-2021
 * IT-4774
 * U3A1
 * 
 */

//Created my main Programmable Thermostat class for the entire program.
public class ProgrammableThermostat extends Application {
    
  @Override
    public void start(Stage stage) throws Exception {
     
        Parent root = FXMLLoader.load(getClass().getResource("ThermFXML.fxml"));
        
        Scene scene = new Scene(root);
       
        
        stage.setScene(scene);
        stage.show();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException, JSONException {
        Thermostat thermostat = new Thermostat();
        thermostat.displayData();
        WriteToFile writer = new WriteToFile();
        ThermSettings settings = new ThermSettings();
        //display the data
        
        
        
        launch(args);

        
        
    }

//    public static void main(String[] args) throws IOException, JSONException {
//
//      //creates a thermostat object from the Thermost class
//      Thermostat thermostat = new Thermostat();
//      WriteToFile writer = new WriteToFile();
//      ThermSettings settings = new ThermSettings();
//      //display the data
//      thermostat.displayData();
//
//      System.out.println("");
//      System.out.println("Would you like to change your settings? (y/n)");
//      Scanner input = new Scanner(System.in);
//      String next = input.next();
//      System.out.println(next);
//      if ("y".equals(next)){
//          settings.getSettingsFromUser();
//      } else{
//          System.out.println("Goodbye.");
//      }
//
//    }
}