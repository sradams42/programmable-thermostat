/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

import java.io.IOException;
import java.util.Date;
import java.util.Scanner;
import org.json.JSONException;
import programmablethermostat.ThermWS;

/**
 *
 * @author Samantha Adams
 */

public class Thermostat {
    
    //Declaring all my thermostat properties.
    String identifier = new String();
    String thermTime = new String();
    String utcTime = new String();
    String name = new String();
    int actHumidity = 0;
    int actTemperature = 0;
    int code = 0;
    String message = new String();
    int targetHumidity = 0;
    int targetTemperature = 0;
    
    Thermostat() {
       
    }
    
    //getters and setters for thermostat properties
    public String getIndentifier() {
        return identifier;
    }
    
    public void setIdentifier(String identifier) {
        this.identifier = identifier;
    }

    public String getThermTime() {
        return thermTime;
    }

    public void setThermTime(String thermTime) {
        this.thermTime = thermTime;
    }

    public String getUtcTime() {
        return utcTime;
    }

    public void setUtcTime(String utcTime) {
        this.utcTime = utcTime;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getActHumidity() {
        return actHumidity;
    }

    public void setActHumidity(int actHumidity) {
        this.actHumidity = actHumidity;
    }

    public int getActTemperature() {
        return actTemperature;
    }

    public void setActTemperature(int actTemperature) {
        this.actTemperature = actTemperature;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
    
    public int getTargetHumidity() {
        return targetHumidity;
    }

    public void setTargetHumidity(int targetHumidity) {
        this.targetHumidity = this.targetHumidity;
    }
    
    public int getTargetTemperature() {
        return targetTemperature;
    }

    public void setTargetTemperature(int targetTemperature) {
        this.targetTemperature = targetTemperature;
    }
    
    //class method to get data from a Web Service
    public void getDataFromService() throws IOException, JSONException {
        //creates an instance of the web service object    
        ThermWS service = new ThermWS();
        //requests that the webservice go get the data and assign to the properties.
        service.setThermostat(this);
    }
    
    //class method to display the data
    public void displayData() throws IOException, JSONException{
        //checks to see if the thermostat already has data.
        if (this.identifier.isEmpty()){
            getDataFromService();
        }
        //displays the data to the console.
        System.out.println("Thermostat Data: ");
        System.out.println("Identifier: " + this.identifier);
        System.out.println("Thermostat Time: " + this.thermTime);
        System.out.println("UTC Time: " + this.utcTime);
        System.out.println("Name: " + this.name);
        System.out.println("Actual Humidity: " + this.actHumidity);
        System.out.println("Actual Temperature: " + this.actTemperature);
        System.out.println("Code: " + this.code);
        System.out.println("Message: " + this.getMessage());
  }
    
    public void saveSettings() throws IOException{
        WriteToFile writer = new WriteToFile();
        try {
            writer.WriteToFile(this);
            System.out.println("Settings saved successfully.");
            System.out.println();
            System.out.println("Would you like to view your new settings? (y/n)");
            
            Scanner input = new Scanner(System.in);
            String view = input.nextLine();
            if ("y".equals(view)){
                ThermSettings settings = new ThermSettings();
                settings.getSettingsFromFile();
                System.out.println("Goodbye");
            }else {
                System.out.println("Goodbye");
            }
        } catch (Exception e) {
            System.out.println("Something went wrong.");
            System.out.println(e);
        }
    }
}
