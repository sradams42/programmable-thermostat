/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Date;
import java.util.Scanner;

/**
 *
 * @author srada
 */
public class ThermSettings {
    //update
    public void getSettingsFromUser() throws IOException{
        Scanner input = new Scanner(System.in);
        System.out.println("Enter Thermostat Name: ");
        String name = input.nextLine();
        System.out.println("Enter Thermostat ID: ");
        String id = input.nextLine();
        System.out.println("Enter a Target Humidity: ");
        String th = input.nextLine();
        int targetHumidity = Integer.parseInt(th);
        System.out.println("Enter a Target Temperature: ");
        String tt = input.nextLine();
        int targetTemperature = Integer.parseInt(tt);
        System.out.println("Actual Humidity: ");
        String ah = input.nextLine();
        int actHumidity = Integer.parseInt(ah);
        System.out.println("Actual Temperature: ");
        String at = input.nextLine();
        int actTemp = Integer.parseInt(at);
        System.out.println("Enter a Code: ");
        String cd = input.nextLine();
        int code = Integer.parseInt(cd);
        System.out.println("Enter a message: ");
        String message = input.nextLine();
        System.out.println("Enter Thermostat Time (YYYY-MM-DD HH:MM:SS): ");
        String thermTime = input.nextLine();
        System.out.println("Enter thermostat UTC Time: ");
        String utcTime = input.nextLine();
        
        Thermostat newTherm = new Thermostat();
        
        newTherm.setIdentifier(id);
        newTherm.setName(name);
        newTherm.setThermTime(thermTime);
        newTherm.setUtcTime(utcTime);
        newTherm.setActHumidity(actHumidity);
        newTherm.setActTemperature(actTemp);
        newTherm.setCode(code);
        newTherm.setMessage(message);
        newTherm.setTargetHumidity(targetHumidity);
        newTherm.setTargetTemperature(targetTemperature);
        
        newTherm.saveSettings();
        }
    
    public void getSettingsFromFile() throws FileNotFoundException{
        try{
            File settings = new File("settings.txt");
            Scanner reader = new Scanner(settings);
            while (reader.hasNextLine()){
                String data = reader.nextLine();
                System.out.println(data);
            }
            reader.close();
        }catch(FileNotFoundException e){
            System.out.println("Houston, we have a problem");
            System.out.println(e);
        }
        
    }
    
}
