/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author srada
 */
public class WriteToFile {
    
    public static void WriteToFile (Thermostat thermostat) throws IOException { 
        
        FileWriter writer = new FileWriter("settings.txt");
        writer.write("ID: " + thermostat.identifier +"\n");
        
        writer.write("Name: " + thermostat.name +"\n");
        writer.write("Actual Humidity: " + thermostat.actHumidity +"\n");
        writer.write("Target Humidity: " + thermostat.targetHumidity + "\n");
        writer.write("Actual Temperature: " + thermostat.actTemperature +"\n");
        writer.write("Target Temperature: " + thermostat.targetTemperature + "\n");
        writer.write("Code: " + thermostat.code +"\n");
        writer.write("Message: " + thermostat.message +"\n");
        writer.write("Thermostat Time: " + thermostat.thermTime +"\n");
        writer.write("UTC Time: " + thermostat.utcTime +"\n");
        writer.close();
    
    }
    
    public void writeScheduleToFile (Scheduler scheduler) throws IOException {
        FileWriter writer = new FileWriter("schedule.txt");
        writer.write("Day of Week: " + scheduler.getDayOfWeek());
        writer.write("Time of Day: " + scheduler.getTimeOfDay());
        writer.write("Current Setting: " + String.valueOf(scheduler.getSettingTemp()));
        writer.close();
    }
    
}
