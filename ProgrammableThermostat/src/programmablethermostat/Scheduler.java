/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;

/**
 *
 * @author Samantha Adams
 */
public class Scheduler {
    
    String dayOfWeek = "";
    String timeOfDay = "";
    int settingTemp = 0;

    public Scheduler(String dayOfWeek, String timeOfDay, int settingTemp) {
        this.dayOfWeek = dayOfWeek;
        this.timeOfDay = timeOfDay;
        this.settingTemp = settingTemp;
    }

    public String getDayOfWeek() {
        return dayOfWeek;
    }

    public void setDayOfWeek(String dayOfWeek) {
        this.dayOfWeek = dayOfWeek;
    }

    public String getTimeOfDay() {
        return timeOfDay;
    }

    public void setTimeOfDay(String timeOfDay) {
        this.timeOfDay = timeOfDay;
    }

    public int getSettingTemp() {
        return settingTemp;
    }

    public void setSettingTemp(int settingTemp) {
        this.settingTemp = settingTemp;
    }
    
    
}
