package programmablethermostat;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import java.io.IOException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.util.Calendar;
import java.util.Date;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.fxml.Initializable;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.SingleSelectionModel;
import javafx.scene.control.Tab;
import javafx.scene.control.TabPane;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;
import org.json.JSONException;

/**
 * FXML Controller class
 *
 * @author Samantha Adams
 */
public class ThermController implements Initializable {
    
    @FXML
    AnchorPane AnchorPane;
    
    @FXML 
    TabPane tabPane;
    
    @FXML
    Tab thermInfo;
    
    @FXML
    Tab settings;

    @FXML
     Label currentDate;
    @FXML
    private Label currentDegree;
    @FXML
    private Label currentDegree2;
    @FXML
     Label currentTemp;
    @FXML
     Label settingsTemp;
    @FXML
     Label lblHumidity;
    @FXML
     Label lblFan;
    @FXML
     Label lblMode;
    @FXML
    private Button btnFan;
    @FXML
    private Button btnSystem;
    @FXML
    private Button btnUp;
    @FXML
    private Button btnDown;
    @FXML
    private Button btnFarenheit;
    @FXML
    private Button btnCelsius;
    @FXML
    private Button update;
    
    
    Thermostat thermostat;
    @FXML
    private Label lblDayOfWeek;
    @FXML
    private Label lblTimeOfDay;
    @FXML
    private Label lblCurrentSetting;
    @FXML
    private Button btnUpSet;
    @FXML
    private Button btnDownSet;
    @FXML
    private Button btnSave;
    @FXML
    private Button btnDay;
    @FXML
    private Label lblTimeOfDay1;
    @FXML
    private Button btnTime;
    @FXML
    private Button btnChangeAMPM;
    @FXML
    private Button reset;
    @FXML
    private Label lblError;
    @FXML
    private Label lblErrorTemp;

    /**
     * Initializes the controller class.
     */


    @FXML
    private void changeFan(ActionEvent event) {
        String value = lblFan.getText();
        switch(value){
            case "Auto":
                lblFan.setText("High");
                break;
            case "High":
                lblFan.setText("Med");
                break;
            case"Med":
                lblFan.setText("Low");
                break;
            case"Low":
                lblFan.setText("Auto");
                break;
            default:
                lblFan.setText("Auto");
        }
    }

    @FXML
    private void changeMode(ActionEvent event) {
        String value = lblMode.getText();
        switch(value){
            case "Cool":
                lblMode.setText("Off");
                break;
            case "Off":
                lblMode.setText("Heat");
                break;
            case"Heat":
                lblMode.setText("Cool");
                break;
            default:
                lblMode.setText("Off");
                break;
        }
        
    }

    @FXML
    private void changeSetTemp(ActionEvent event) {
        String value = ((Button) event.getSource()).getText();
        System.out.println(value);
        String tempStr = ((String) settingsTemp.getText());
        int temp = Integer.parseInt(tempStr);
        System.out.println(temp);
        if(value.equals("+")){
        
            temp = temp + 1;
            System.out.println(temp);
        } else{
            temp = temp - 1;
        }
        tempStr = String.valueOf(temp);
        settingsTemp.setText(tempStr);
    }
    
    private int convertF(int temp){
        Double t = Double.valueOf(temp);
        t = (t*(9.0/5.0))+32.0;
        temp = (int) Math.round(t);
        return temp;
    }

    private int convertC(int temp){
        Double t = Double.valueOf(temp);
        t = (t-32.0)*(5.0/9.0);
        temp = (int) Math.round(t);
        System.out.println(temp);
        return temp;
    }
    
    @FXML
    private void convertTemp(ActionEvent event) {
        String value = ((Button) event.getSource()).getText();
        String currentDeg = ((String) currentDegree.getText());
        String tempStr = ((String) currentTemp.getText());
        String setTemp = ((String) settingsTemp.getText());
        
        int temp = Integer.parseInt(tempStr);
        int setT = Integer.parseInt(setTemp);
        int curr =0;
        int set = 0;
        
        if(value.equals("°C") && !currentDeg.equals("°C")){
            curr = convertC(temp);
            set = convertC(setT);
            currentDegree.setText("°C");
            currentDegree2.setText("°C");
        }else if(value.equals("°F") && !currentDeg.equals("°F")){
            curr = convertF(temp);
            set = convertF(setT);
            currentDegree.setText("°F");
            currentDegree2.setText("°F");
        }else{
            
        }
        String currStr = String.valueOf(curr);
        String setStr = String.valueOf(set);
        currentTemp.setText(currStr);
        settingsTemp.setText(setStr);
    }
    @FXML
    private void updateThermostat() throws IOException, JSONException {
        Thermostat thermostat = new Thermostat();
        thermostat.getDataFromService();
        currentTemp.setText("00");
        lblHumidity.setText(String.valueOf(thermostat.getActHumidity())+"%");
        currentDate.setText(thermostat.getThermTime());
        currentTemp.setText(String.valueOf(thermostat.getActTemperature()));
        
    }

    @Override
    public void initialize(URL url, ResourceBundle rb) {

    }

    @FXML
    private void changeDay(ActionEvent event) {
        String value = ((Button) event.getSource()).getText();
        System.out.println(value);
        String day = lblDayOfWeek.getText();
        System.out.println(day.equals("--Set Day--"));
        if (day.equals("--Set Day--")){
            lblDayOfWeek.setText("Sunday");
        }
        System.out.println(day);
        
        switch(value){
            case "+":
                switch(day){
                    case "Sunday":
                        lblDayOfWeek.setText("Monday");
                        break;
                    case "Monday":
                        lblDayOfWeek.setText("Tuesday");
                        break;
                    case"Tuesday":
                        lblDayOfWeek.setText("Wednesday");
                        break;
                    case"Wednesday":
                        lblDayOfWeek.setText("Thursday");
                        break;
                    case "Thursday":
                        lblDayOfWeek.setText("Friday");
                        break;
                    case "Friday":
                        lblDayOfWeek.setText("Saturday");
                        break;
                    case "Saturday":
                        lblDayOfWeek.setText("Sunday");
                        break;
                    default:
                        lblDayOfWeek.setText("Sunday");
                } 
                break;
            case "-":
                switch(day){
                    case "Sunday":
                        lblDayOfWeek.setText("Saturday");
                        break;
                    case "Monday":
                        lblDayOfWeek.setText("Sunday");
                        break;
                    case"Tuesday":
                        lblDayOfWeek.setText("Monday");
                        break;
                    case"Wednesday":
                        lblDayOfWeek.setText("Tuesday");
                        break;
                    case "Thursday":
                        lblDayOfWeek.setText("Wednesday");
                        break;
                    case "Friday":
                        lblDayOfWeek.setText("Thursday");
                        break;
                    case "Saturday":
                        lblDayOfWeek.setText("Friday");
                        break;
                    default:
                        lblDayOfWeek.setText("Sunday");
                }
                break;
            default:
                lblDayOfWeek.setText("Sunday");
        }
    }

    @FXML
    private void changeTime(ActionEvent event) throws ParseException {
        
        String btnTxt = ((Button) event.getSource()).getText();
        String value = lblTimeOfDay.getText();
        System.out.println(value);
        if (value.equals("--Set Time--")){
            lblTimeOfDay.setText("12:00");
            value = lblTimeOfDay.getText();
            System.out.println(value);
        }
        SimpleDateFormat d = new SimpleDateFormat("HH:mm");
        Date date = d.parse(value);
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        if(btnTxt.equals("+")){
            cal.add(Calendar.MINUTE, 1);
            String val = d.format(cal.getTime());
            lblTimeOfDay.setText(val);
        } else{
            cal.add(Calendar.MINUTE, -1);
            String val = d.format(cal.getTime());
            lblTimeOfDay.setText(val);
        }
    }

    @FXML
    private void changeCurrentSet(ActionEvent event) {
        String value = ((Button) event.getSource()).getText();
        String tempStr = ((String) lblCurrentSetting.getText());
        if (tempStr.equals("--Set Temp--")) {
            lblCurrentSetting.setText(settingsTemp.getText());
            tempStr = lblCurrentSetting.getText();
        }
        Integer t = Integer.parseInt(tempStr);
            
        if (t > 50 && t<90){
            if(value.equals("+")){
        
            t = t + 1;
            System.out.println(t);
            } else{
                t = t - 1;
                System.out.println(t);
            }
        } else if (t<=50){
            if(value.equals("+")){
        
            t = t + 1;
            lblErrorTemp.setText("");
            }
            else{
                lblErrorTemp.setText("Min limit reached.");
            }
        } else if (t>=90){
            if(value.equals("-")){
        
            t = t - 1;
            lblErrorTemp.setText("");
            }
            else{
                lblErrorTemp.setText("Max limit reached.");
            }
        } else {   
        }
        tempStr = String.valueOf(t);
        lblCurrentSetting.setText(tempStr);
    }
    
    @FXML
    private void reset(ActionEvent event){
        lblDayOfWeek.setText("--Set Day--");
        lblTimeOfDay.setText("--Set Time--");
        lblCurrentSetting.setText("--Set Temp--");
        lblError.setText("");
        lblErrorTemp.setText("");
        
    }

    @FXML
    private void saveSettings(ActionEvent event) throws IOException {
        System.out.println(lblDayOfWeek.getText().equals("--Set Day--"));
        System.out.println(lblCurrentSetting.getText().equals("--Set Temp--"));
        System.out.println(lblTimeOfDay.getText().equals("--Set Time--"));
    
        if (lblDayOfWeek.getText().equals("--Set Day--")){
            if(lblError.getText().equals("")){
                lblError.setText("Missing Day");
            } else {
                lblError.setText(lblError.getText()+", Day");
            }
        }
        if (lblCurrentSetting.getText().equals("--Set Temp--")){
            if(lblError.getText().equals("")){
                lblError.setText("Missing Temp");
            } else {
                lblError.setText(lblError.getText()+", Temp");
            }
        }
        if (lblTimeOfDay.getText().equals("--Set Time--")){
            if(lblError.getText().equals("")){
                lblError.setText("Missing Time");
            } else {
                lblError.setText(lblError.getText()+", Time");
            }
        }
        
        if(lblError.getText().equals("")){
            int temp = Integer.parseInt(lblCurrentSetting.getText());
            Scheduler scheduler = new Scheduler(lblDayOfWeek.getText(), lblTimeOfDay.getText(), temp);
            WriteToFile writerx = new WriteToFile();
            writerx.writeScheduleToFile(scheduler);
            lblError.setText("Settings saved successfully.");
        } else {
            lblError.setText(lblError.getText()+" . Cannot save.");
        }
    }

    @FXML
    private void changeTab(ActionEvent event) throws IOException {
        
        tabPane.getSelectionModel().selectPrevious();
   
    }
}
