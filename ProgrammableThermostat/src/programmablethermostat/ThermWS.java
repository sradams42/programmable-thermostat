/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package programmablethermostat;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.net.URL;
import java.nio.charset.Charset;
import javax.jws.WebService;
import javax.jws.WebMethod;
import java.util.Date;
import org.json.JSONException;
import org.json.JSONObject;
/**
 *
 * @author Samantha Adams
 */

//creating the webservice class for the temperature
@WebService
public class ThermWS {
    //class constructor
    public ThermWS(){
        
    }
    //converting reader data to a string.
    private static String dataToString(Reader reader) throws IOException {
        //creates a new String builder object
        StringBuilder builder = new StringBuilder();
            //defines the index
        int index;
            //iterates through the data until there's nomore and appends each char to the string
        while ((index = reader.read()) != -1) {
          builder.append((char) index);
        }
        return builder.toString();
    }
    //gets the data from the URL and converts to a JSON Object
    @WebMethod
    public JSONObject getDataFromURL() throws IOException, JSONException {
            //try and catch to handle not found errors. Will throw a file not found error if the url does not work.
        try(InputStream stream = new URL("http://media.capella.edu/BBCourse_Production/IT4774/temperature.json").openStream(); ){
            BufferedReader reader = new BufferedReader(new InputStreamReader(stream, Charset.forName("UTF-8")));
            String jsonText = dataToString(reader);
            JSONObject json = new JSONObject(jsonText);
            return json; 
        }
        catch(Exception e){
            System.out.println("Unable to retrieve data from Web Service.");
            System.out.println(e);
            return null;
        }
            
    }
    
    //sets the thermostat data
    public void setThermostat(Thermostat thermostat) throws IOException, JSONException{
        
        JSONObject json = getDataFromURL();
        JSONObject runtime = json.getJSONObject("runtime");
        JSONObject status = json.getJSONObject("status");
        
        thermostat.setIdentifier((String) json.get("identifier"));
        thermostat.setThermTime((String) json.get("thermostatTime"));
        thermostat.setUtcTime((String) json.get("utcTime"));
        thermostat.setName((String) json.get("name"));
        thermostat.setActHumidity(runtime.getInt("actualHumidity"));
        thermostat.setActTemperature(runtime.getInt("actualTemperature"));
        thermostat.setCode(status.getInt("code"));
        thermostat.setMessage((String) status.get("message"));
    }
    
}
